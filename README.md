## Packages

This role:

  * installs a list of packages
  * removes a list of packages
  * updates debian cache on debian systems

## Role parameters

| name                  | value       | optionnal | default_value   | description                       |
| ----------------------|-------------|-----------|-----------------|-----------------------------------|
| packages_list         | list        | yes       | []              | list of packages to be installed  |
| packages_to_remove    | list        | yes       | []              | list of packages to be removed    |
| packages_update_cache | boolean     | yes       | false           | update system packages cache      |


## Using this role

### ansible galaxy
Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/ansible-roles3/packages
  scm: git
  version: v1.1
```

and adjust the version field with the [latest release](https://gitlab.com/ansible-roles3/packages/-/releases).

### Sample playbook

```yaml
---
- hosts: all
  roles:
      - role: packages
        packages_list: [ figlet, cowsay ]
        packages_to_remove:
          - tcpdump
          - nano
        packages_update_cache: true
```

## Tests

[tests/tests_packages](tests/tests_packages)
